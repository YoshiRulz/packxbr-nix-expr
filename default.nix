{ pkgs ? builtins.import (builtins.fetchTarball "https://github.com/NixOS/nixpkgs/archive/22.11.tar.gz") {}
, lib ? pkgs.lib
, stdenv ? pkgs.stdenvNoCC
, fetchFromGitHub ? pkgs.fetchFromGitHub
, fetchzip ? pkgs.fetchzip
, makeWrapper ? pkgs.makeWrapper
, python3 ? pkgs.python3
}: let
	xBRZScalerTest = stdenv.mkDerivation {
		pname = "ScalerTest";
		version = "1.1";
		src = fetchzip {
			url = "mirror://sourceforge/xbrz/ScalerTest_1.1.zip";
			stripRoot = false; # this is a zipbomb
			hash = "sha256-/ISTs1tRe0v+GjnNRaupNfdAXkoxUVzRzC+PWQRYJFY=";
		};
		dontConfigure = true;
		dontBuild = true;
		#TODO rundeps (GTK, X, pango, libpng, libjpeg, libtiff) for NixOS
		installPhase = ''
			runHook preInstall
			mkdir -p $out/bin
			cp -vT ScalerTest_Linux $out/bin/ScalerTest
			chmod +x $out/bin/ScalerTest
			mkdir -p $out/share
			cp -avT Samples $out/share/xbrz
			runHook postInstall
		'';
		meta = {
			description = "Image upscaler (GUI) using the xBRZ reference implementation";
			homepage = "http://sourceforge.net/projects/xbrz/";
			license = lib.licenses.gpl3Only;
			sourceProvenance = lib.sourceTypes.binaryNativeCode;
		};
	};
	pythonEnv = python3.withPackages (ps: builtins.attrValues {
		inherit (ps) numpy pillow;
#		inherit (ps) aioshutil; # shutil?
#		inherit (ps) ; # glob?
#		inherit (ps) ; # zipfile?
#		inherit (ps) ; # subprocess?
#		inherit (ps) ; # webbrowser?
	});
in stdenv.mkDerivation {
	pname = "PackXBR";
	version = "1.1.5+f3d55cda0";
	src = fetchFromGitHub {
		owner = "CodeF53";
		repo = "PackXBR";
		rev = "f3d55cda0001197986a4cb9cfe1de3a1b34bb1fb";
		hash = "sha256-qGZPFocf1TG0Op9rX7h+AXwUj2QtPQUIsg89oCZnaeQ=";
	};
	postPatch = ''
		sed -i 's@ScalerTest_Windows.exe@${xBRZScalerTest}/bin/ScalerTest@g' *.py
	'';
	nativeBuildInputs = [ makeWrapper ];
	buildInputs = [ pythonEnv ];
	dontConfigure = true;
	dontBuild = true;
	installPhase = ''
		runHook preInstall
		mkdir -p $out/bin $out/share/PackXBR
		cp -avt $out/share/PackXBR *
		makeWrapper ${lib.getBin pythonEnv}/bin/python $out/bin/PackXBR \
			--add-flags $out/share/PackXBR/main.py
		runHook postInstall
	'';
	meta = {
		description = "A utility for automatic application of the XBR scaling algorithm to game textures";
		homepage = "https://github.com/CodeF53/PackXBR";
		license = lib.licenses.gpl3Only; # source has no license but the library's license doesn't allow that
	};
}
